﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Data.Json;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace HttpDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private HttpClient httpClient;

        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;

            httpClient = new HttpClient();
            httpClient.MaxResponseContentBufferSize = 256000;
            
        }

        private async void Start_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                string responseBodyAsText;
                JsonData.Text = "";
                status.Text = "Waiting for response ...";

                HttpResponseMessage response = await httpClient.GetAsync("http://docs.blackberry.com/sampledata.json");

                HttpResponseMessage response1 = await httpClient.PostAsync("http://192.168.1.17/exact_knowledge_new/services/gets.php?req=login", content).ContinueWith(
                       (postTask) =>
                       {
                           postTask.Result.EnsureSuccessStatusCode();
                       }
                   );

                response.EnsureSuccessStatusCode();

                status.Text = response.StatusCode + " " + response.ReasonPhrase + Environment.NewLine;
                responseBodyAsText = await response.Content.ReadAsStringAsync();
                responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines
                //JsonData.Text = responseBodyAsText;

                JsonArray a1 = JsonArray.Parse(responseBodyAsText);
                for (uint i = 0; i < a1.Count; i++)
                {
                    //JsonParseData.Text = a1.GetObjectAt(i).GetNamedString("vehicleType");
                    NameOfVehicals.Items.Add(a1.GetObjectAt(i).GetNamedString("vehicleType"));
                }
                    

            }
            catch (HttpRequestException hre)
            {
                status.Text = hre.ToString();
            }
            catch (Exception ex)
            {
                // For debugging
                status.Text = ex.ToString();
            }
        }


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

    }
}
